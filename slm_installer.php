<?php
//***** Installer *****
global $wpdb;
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');	

//***Installer variables***
$lic_key_table = SLM_TBL_LICENSE_KEYS;
$lic_domain_table = SLM_TBL_LIC_DOMAIN;
$lic_setting_table = SLM_TBL_LIC_SETTING;

$charset_collate = '';
if (!empty($wpdb->charset)){
    $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
}else{
    $charset_collate = "DEFAULT CHARSET=utf8";
}
if (!empty($wpdb->collate)){
    $charset_collate .= " COLLATE $wpdb->collate";
}
        
$lk_tbl_sql = "CREATE TABLE " . $lic_key_table . " (
      id int(12) NOT NULL auto_increment,
      license_key varchar(255) NOT NULL,      
      PRIMARY KEY (id)
      )" . $charset_collate . ";";
dbDelta($lk_tbl_sql);

$ld_tbl_sql = "CREATE TABLE " .$lic_domain_table. " (
      id INT NOT NULL AUTO_INCREMENT ,
      source_name varchar(255) NOT NULL,
      auth_id int(11) DEFAULT NULL,
      schedule_post varchar(255) NOT NULL,
      no_post int(12) NOT NULL,
      post_status varchar(255) NOT NULL,
      post_sel_type varchar(255) NOT NULL,
      date_created date NOT NULL DEFAULT '0000-00-00',  
      source_type varchar(255) NOT NULL,
      source_category varchar(255) NOT NULL,    
      PRIMARY KEY ( id )
      )" . $charset_collate . ";";
dbDelta($ld_tbl_sql);

$setting_tbl_sql = "CREATE TABLE " .$lic_setting_table. " (
  id INT NOT NULL AUTO_INCREMENT ,
  auth_id int(11) DEFAULT NULL,
  duplicate_post varchar(225) DEFAULT NULL,
  rule_setting_post varchar(225) DEFAULT NULL,
  metabox_post varchar(225) DEFAULT NULL,
  logging_rule varchar(225) DEFAULT NULL,
  detail_logging_rule varchar(225) DEFAULT NULL,
  auto_clear_log varchar(225) DEFAULT NULL,
  user_agent varchar(225) DEFAULT NULL,
  proxy_url varchar(225) DEFAULT NULL,
  proxy_auth varchar(225) DEFAULT NULL,
  timeout_rule_run varchar(225) DEFAULT NULL,
  send_rule_run varchar(225) DEFAULT NULL,
  original_post_content varchar(225) DEFAULT NULL,
  skip_post_import_html varchar(225) DEFAULT NULL,
  skip_post_extract varchar(225) DEFAULT NULL,
  generate_inexist_categories varchar(225) DEFAULT NULL,
  generate_post_content varchar(225) DEFAULT NULL,
  htmltag_generate_post varchar(225) DEFAULT NULL,
  js_crawler_content varchar(225) DEFAULT NULL,
  html_final_content_id varchar(225) DEFAULT NULL,
  html_final_content_class varchar(225) DEFAULT NULL,
  read_more varchar(225) DEFAULT NULL,
  auto_translate_to_global varchar(225) DEFAULT NULL,
  translate_to_global_lang varchar(225) DEFAULT NULL,
  keep_link_source varchar(225) DEFAULT NULL,
  spin_text varchar(225) DEFAULT NULL,
  no_spin_title varchar(225) DEFAULT NULL,
  min_word_title varchar(225) DEFAULT NULL,
  max_word_title varchar(225) DEFAULT NULL,
  min_word_content varchar(225) DEFAULT NULL,
 max_word_content varchar(225) DEFAULT NULL,
  banned_words varchar(225) DEFAULT NULL,
  required_words varchar(225) DEFAULT NULL,
  required_all varchar(225) DEFAULT NULL,
  skip_no_img varchar(225) DEFAULT NULL,
  skip_old varchar(225) DEFAULT NULL,
  no_local_image varchar(225) DEFAULT NULL,
  resize_width varchar(225) DEFAULT NULL,
  resize_height varchar(225) DEFAULT NULL,
  date_created date DEFAULT '0000-00-00',      
      PRIMARY KEY ( id )
      )" . $charset_collate . ";";
dbDelta($setting_tbl_sql);


update_option("wp_lic_mgr_db_version", WP_LICENSE_MANAGER_DB_VERSION);

// Add default options
$options = array(
    'lic_creation_secret' => uniqid('', true),
    'lic_prefix' => '',
    'default_max_domains' => '1',
    'lic_verification_secret' => uniqid('', true),
    'enable_debug' => '',
);
add_option('slm_plugin_options', $options);
