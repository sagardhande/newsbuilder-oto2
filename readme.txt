=== NewsBuilder ===
Contributors: Newsbuilder Team
Donate link: http://newsbuilder.io/
Tags: News, RSS Feeds
Requires at least: 4.6
Tested up to: 4.7
Stable tag: v1.01
Requires PHP: 7.0
License: Commercial


== Changelog == 
v1.01
1. Edit option for NEWS API key
2. Search Article by Category wise
3. Search Article by Country wise
4. Search Article by Keyword
5. Added Update Function