<?php

/*
 * This file only gets included if "is_admin()" check is true.
 * Admin menu rendering code goes in this file.
 */

add_action('admin_menu', 'wp_lic_mgr_add_admin_menu');

//Include menu handling files
require_once(WP_LICENSE_MANAGER_PATH . '/menu/slm-manage-licenses.php');
require_once(WP_LICENSE_MANAGER_PATH . '/menu/add-news.php');
require_once(WP_LICENSE_MANAGER_PATH . '/menu/add-rss.php');
require_once(WP_LICENSE_MANAGER_PATH . '/menu/add-setting.php');
require_once(WP_LICENSE_MANAGER_PATH . '/menu/help-support.php');

function wp_lic_mgr_add_admin_menu() {

    add_menu_page("NewsBuilder", "NewsBuilder", SLM_MANAGEMENT_PERMISSION, SLM_MAIN_MENU_SLUG, "wp_lic_mgr_manage_licenses_menu", SLM_MENU_ICON);

    add_submenu_page(SLM_MAIN_MENU_SLUG, "Register Key", "Register Key", SLM_MANAGEMENT_PERMISSION, SLM_MAIN_MENU_SLUG, "wp_lic_mgr_manage_licenses_menu");

    add_submenu_page(SLM_MAIN_MENU_SLUG, "Latest News To Posts", "Latest News To Posts", SLM_MANAGEMENT_PERMISSION, 'add-news.php', "add_news_function");

    add_submenu_page(SLM_MAIN_MENU_SLUG, "RSS Feeder", "RSS Feeder", SLM_MANAGEMENT_PERMISSION, 'add-rss.php', "add_rss_function");
   
    add_submenu_page(SLM_MAIN_MENU_SLUG, "Setting", "Setting", SLM_MANAGEMENT_PERMISSION, 'add-setting.php', "add_setting_function");

     add_submenu_page(SLM_MAIN_MENU_SLUG, "Help & Support", "Help & Support", SLM_MANAGEMENT_PERMISSION, 'help-support.php', "wp_help_support");


 	



   
}
