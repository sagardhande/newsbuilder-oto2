<?php

class WPLM_List_Licenses extends WP_License_Mgr_List_Table {
    
    function __construct(){
        global $status, $page,$wpdb;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'item',     //singular name of the listed records
            'plural'    => 'items',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


 function prepare_items_list() {
        


if (isset($_POST['news_rules_list2']) || isset($_POST['news_rules_list'])) { 

    global $wpdb;
    global $current_user;
    extract($_POST);

    //print_r($_POST);exit;

    if($_POST['news_rules_list2'])
    {
        $news_rules_list = $_POST['news_rules_list2'];
    }
    else
    {
        $news_rules_list = $_POST['news_rules_list'];
    }


    if($_POST['news_rules_list_category'])
    {
        $news_rules_cat = $_POST['news_rules_list_category'];
    }
    else
    {
        $news_rules_cat = 'Uncategorized';
    }


    /***************Action Tab******************************************/

    $article_source_table = $wpdb->prefix . "article_source_tbl";
    $lic_key_tbl = $wpdb->prefix . "lic_key_tbl";
    $setting_tbl = $wpdb->prefix . "setting_tbl";
 
    $flag = 0;

    if(isset($_POST['postid']))
    {


        if($_POST['post_action'] == 'run_this_rule_now')
        {
            $flag = 1;
            $sql = "UPDATE $article_source_table SET source_name = '$news_rules_list', schedule_post = '$post_schedule', no_post ='$no_post',post_status = '$post_status', post_sel_type = '$post_sel_type',date_created = CURRENT_TIMESTAMP WHERE id = '".$_POST['postid']."'";
            $wpdb->query($sql);
            $insertid = $_POST['postid'];
        }
        elseif($_POST['post_action'] == 'delete_this_rule')
        {          
            $sql = "DELETE FROM $article_source_table WHERE id = '".$_POST['postid']."'";
            $wpdb->query($sql);
            $sql = "DELETE FROM $wpdb->posts WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql); 
        }
        elseif($_POST['post_action'] == 'duplicate_this_rule')
        {   
            $flag = 3;       
            $sql = "INSERT INTO $article_source_table (source_name, schedule_post, no_post,post_status, post_sel_type,date_created,source_category,source_type) VALUES ('$news_rules_list', '$post_schedule', '$no_post', '$post_status','$post_sel_type', CURRENT_TIMESTAMP,'$news_rules_list_category2','NEWS')";
            $wpdb->query($sql);
            $insertid = $wpdb->insert_id;
        }
        elseif($_POST['post_action'] == 'permanently_delete_all_posts')
        {          
            $sql = "DELETE FROM $wpdb->posts WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql); 
        }
        elseif($_POST['post_action'] == 'move_all_posts_to_trash')
        {          
            $sql = "UPDATE $wpdb->posts SET post_status = 'trash' WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql);
        }
    }
    else
    {   
         $flag = 2;     
        $sql = "INSERT INTO $article_source_table (source_name, schedule_post, no_post,post_status, post_sel_type,date_created,source_type,source_category) VALUES ('$news_rules_list', '$post_schedule', '$no_post', '$post_status','$post_sel_type', CURRENT_TIMESTAMP,'NEWS','$news_rules_cat')";
        $wpdb->query($sql);
        $insertid = $wpdb->insert_id;
    }
      

    /***********************Action Tab************************************/

    $result_setting = $wpdb->get_results("SELECT * FROM $setting_tbl ORDER BY id DESC");
    $lang_trans = $result_setting[0]->translate_to_global_lang; 
    $spin__text = $result_setting[0]->spin_text; 

    $results = $wpdb->get_results("SELECT license_key FROM $lic_key_tbl");
    $newlicence = $results[0]->license_key;  


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  

    if($_POST['search_artical_title'])
    {
    curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/everything?q='.$_POST['search_artical_title'].'&sortBy=publishedAt&apiKey='.$newlicence);   
   
    }
    elseif($_POST['news_rules_category']) 
    {  
        if($_POST['post_sel_country'])
        {
         curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?category='.$_POST['news_rules_category'].'&country='.$_POST['post_sel_country'].'&apiKey='.$newlicence);    
        }
        else{
          curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?category='.$_POST['news_rules_category'].'&apiKey='.$newlicence);
        }
    }
    elseif($_POST['news_rules_category'] =="" && $_POST['post_sel_country']!="") 
    {         
         curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?country='.$_POST['post_sel_country'].'&apiKey='.$newlicence); 
    }
    else
    {  
     curl_setopt($ch, CURLOPT_URL, 'https://newsapi.org/v2/top-headlines?sources='.$news_rules_list.'&apiKey='.$newlicence);
    }

    $result = curl_exec($ch);
    curl_close($ch);
    $obj = json_decode($result);

    if($_POST['news_rules_list_category'])
    {
      $selected_cat = $_POST['news_rules_list_category'];
    }  
    else
    {
       $selected_cat = $_POST['news_rules_list_category2'];
    }


    $term = term_exists( $selected_cat, 'category' );
    
    if(count($term) > 0)
     {
       $wpdocs_cat_id = $term['term_id'];
     }
     else{
    $wpdocs_cat = array('cat_name' => $_POST['news_rules_list_category'], 'category_description' => $_POST['news_rules_list_category'] , 'category_nicename' => $_POST['news_rules_list_category'], 'category_parent' => '');
    $wpdocs_cat_id = wp_insert_category($wpdocs_cat);
    }

 
  if(count($obj->articles) && ($flag==1 || $flag==2 || $flag==3))
  {

   for($i=0;$i<count($obj->articles);$i++)
   {
   
    $apiKey = $result_setting[0]->proxy_url; 

    $text_title = $obj->articles[$i]->title;
    $post__title = str_replace("'", "", $text_title); 

 
    if($lang_trans !='disabled' && $apiKey!="")
    {
        $url_title = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($post__title) . '&target='.$lang_trans;
        $handle = curl_init($url_title);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);                 
        $responseDecoded = json_decode($response, true);
        curl_close($handle);

        $post__title = $responseDecoded['data']['translations'][0]['translatedText'];
    }
    else
    {
        $post__title = $post__title;
    }     
    
    $apiKey = $result_setting[0]->proxy_url; 
    require_once (dirname(__FILE__) . "/readability.php");

    $url = $obj->articles[$i]->url;
    if (!preg_match('!^https?://!i', $url)) $url = 'http://'.$url;

    $html = file_get_contents($url);
    $page_content = new Readability($html, $url);
    $page_content->init();
    $post__content = $page_content->articleContent->innerHTML;

    /***********spin code start*********************/
   
    if($spin__text =='wikisynonyms')
    {
     require_once(dirname(__FILE__) . "/text-spinner.php");
     $phpTextSpinner = new PhpTextSpinner();    
     $spinContent = $phpTextSpinner->spinContent($post__content);
     $translated = $phpTextSpinner->runTextSpinner($spinContent);
     $post__content = $translated;
    }
    else
    {        
     $post__content = $page_content->articleContent->innerHTML;
    }
    
    /***********spin code end *********************/
  
  if($lang_trans !='disabled' && $apiKey!="")
    {
        $text = $post__content;
        
        $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&target='.$lang_trans;

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);                 
        $responseDecoded = json_decode($response, true);
        curl_close($handle);
        $post__content = htmlspecialchars_decode($responseDecoded['data']['translations'][0]['translatedText']);  
    }
    else
    {        
        $post__content = $post__content;
    }

   if($result_setting[0]->read_more !="" && $result_setting[0]->user_agent !="")
     {
        $find__word  = $result_setting[0]->read_more;
        $replace__string = base64_decode($result_setting[0]->user_agent);
        $post__content = str_replace($find__word,$replace__string ,$post__content);
     }
  
   $dublicate_post = $result_setting[0]->duplicate_post; 

   $banned_words = explode(',', $result_setting[0]->banned_words);
   $required_words = explode(',', $result_setting[0]->required_words);


   $flag_badwords = false;
   $flag_requirwords = false;

   $search_for_content = $post__title.' '.$obj->articles[$i]->description;
   
   foreach($banned_words as $badwords)
    {
     if(stristr($search_for_content,$badwords))
      {
       $flag_badwords = true;  
      }     
    }

    foreach($required_words as $requirwords)
    {
     if(stristr($search_for_content,$requirwords))
      {
       $flag_requirwords = true;  
      }      
    }
    

   $post_already = $wpdb->get_results("SELECT * FROM $wpdb->posts where post_title ='".ltrim($post__title)."'");

   $min_word_title = $result_setting[0]->min_word_title;
   $max_word_title = $result_setting[0]->max_word_title;
   $str__count = str_word_count($post__title);
   

   $min_word_content = $result_setting[0]->min_word_content;
   $max_word_content = $result_setting[0]->max_word_content;
   $str__count_content = str_word_count($post__content);


 
    if((count($post_already) == 0)  || ($dublicate_post != "on") )
    {
        if($flag_badwords == false || ($result_setting[0]->banned_words == "" ))
        {   
           if($flag_requirwords == true || ($result_setting[0]->required_words == "" )) 
            {
                if(($str__count > $min_word_title)  || ($min_word_title == ""))
                 { // $min_word_title
                    if(($str__count < $max_word_title)  || ($max_word_title == ""))
                     { // $max_word_title
                       if(($str__count_content > $min_word_content)  || ($min_word_content == ""))
                       { // $min_word_title
                          if(($str__count_content < $max_word_content)  || ($max_word_content == ""))
                           { // $max_word_title


        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);

        $articles__thumbnail =="";
        $articles__thumbnail = $image['src'][0];
              
                                  
       if(($result_setting[0]->skip_no_img =="on"))
        {
          if($articles__thumbnail !="")
          {

          $my_post = array(
          'post_title'    => $post__title,
          'post_content'  => $post__content,
          'post_status'   => $_POST['post_status'],
          'post_excerpt'   => $obj->articles[$i]->url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $_POST['post_sel_type']
        
        );              
        $postid = wp_insert_post($my_post);

        $sql = "UPDATE $wpdb->posts SET source_post_id = ".$insertid." WHERE ID = '".$postid."'";
        $wpdb->query($sql);
         
        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );

          } // no image
        } // if no image

        else
        {
          // take all images

        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);
        $articles__thumbnail = $image['src'][0];

        if($articles__thumbnail =="")
        {
            $articles__thumbnail = "https://dummyimage.com/600x400/b6c9d4/fff.png&text=No+Image+Available.!";
        }
        else
        {
            $articles__thumbnail = $image['src'][0];
        }


          $my_post = array(
          'post_title'    => $post__title,
          'post_content'  => $post__content,
          'post_status'   => $_POST['post_status'],
          'post_excerpt'   => $obj->articles[$i]->url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $_POST['post_sel_type']
        
        );              
        $postid = wp_insert_post($my_post);

        $sql = "UPDATE $wpdb->posts SET source_post_id = ".$insertid." WHERE ID = '".$postid."'";
        $wpdb->query($sql);
         
        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );

        }


                  } 
                 } 
                } 
              } 
            } 
         } 

        } 

        if($i==$_POST['no_post'])
            break;
        
    } 
   

  } 
  elseif(count($obj->articles) && ($flag==0))
    {
      
    }
else
    {
       
    }

   //else
     echo '<script type="text/javascript">location.reload(true);</script>';

       
        }
            
       
    }
}